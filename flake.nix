{
  description = "Fira Code, Italics edition";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      with import nixpkgs { system = "x86_64-linux"; };
      let
        pkgs = nixpkgs.legacyPackages.${system};
        firaCode = pkgs.fetchzip {
          name = "fira-code";
          url =
            "https://github.com/tonsky/FiraCode/releases/download/6.2/Fira_Code_v6.2.zip";
          stripRoot = false;
          sha256 = "UHOwZL9WpCHk6vZaqI/XfkZogKgycs5lWg1p0XdQt0A=";
        };
        package = stdenv.mkDerivation {
          name = "firaCodeItalics";

          src = self;

          propagatedNativeBuildInputs = with pkgs; [
            python313Packages.fontforge
            python313
          ];

          buildPhase = ''
            mkdir output
            python3 $src/build.py ${firaCode}/ttf/FiraCode-Bold.ttf
            python3 $src/build.py ${firaCode}/ttf/FiraCode-Light.ttf
            python3 $src/build.py ${firaCode}/ttf/FiraCode-Medium.ttf
            python3 $src/build.py ${firaCode}/ttf/FiraCode-Regular.ttf
            python3 $src/build.py ${firaCode}/ttf/FiraCode-Retina.ttf
            python3 $src/build.py ${firaCode}/ttf/FiraCode-SemiBold.ttf
          '';

          installPhase = ''
            mkdir -p $out/share/fonts
            cp -R output $out/share/fonts/ttf
          '';
        };
      in { packages.default = package; });
}
