#!/usr/bin/env python3

import fontforge
import psMat
import sys
import os
import math


def warning(text):
    print("\x1b[33mWARNING:\x1b[0m", text)


def gw(glyph, code, warning_str):
    if glyph.validation_state & code == code:
        warning("Glyph `" + glyph.glyphname + "` " + warning_str)


def get_output_filename(font_filename: str) -> str:
    split = os.path.splitext(font_filename)
    return split[0] + "-Italic" + split[1]


font_path = sys.argv[1]
font_filename = os.path.basename(font_path)
font = fontforge.open(font_path)
output_filename = get_output_filename(font_filename)
font.selection.all()
font.transform(psMat.skew(12 / 360 * 2 * math.pi))

for glyph in font.glyphs():
    name = glyph.glyphname
    # font.selection.select(glyph)
    # font.italicize(italic_angle=-12, a=True, f=1)
    # font.selection.none()
    glyph.validate()
    if glyph.validation_state & 0x1 == 0:
        warning(f"Glyph `{name}` has no validation")
    else:
        gw(glyph, 0x2, "has an open contour")
        gw(glyph, 0x4, "has an intersect")
        gw(glyph, 0x8, "has 1+ contour drawn in the wrong direction")
        gw(glyph, 0x10, "has a reference that has been flipped")
        gw(glyph, 0x20, "has missing extrema")
        gw(glyph, 0x40, "has opentype table shananigans")
        gw(
            glyph,
            0x40000,
            "has Points too far appart. Coordinates must be within 32767",
        )

        # TrueType only, errors in original file

        gw(glyph, 0x400, "has more points in a glyph than allowed in 'maxp'")
        gw(glyph, 0x800, "has more paths in a glyph than allowed in 'maxp'")
        gw(glyph, 0x1000, "has more points in a composite glyph than allowed in 'maxp'")
        gw(glyph, 0x2000, "has more paths in a composite glyph than allowed in 'maxp'")
        gw(glyph, 0x4000, "has instructions longer than allowed in 'maxp'")
        gw(glyph, 0x8000, "has more references in a glyph than allowed in 'maxp'")
        gw(glyph, 0x10000, "has references nested more deeply than allowed in 'mapx'")
        gw(glyph, 0x40000, "has points too gar appart")
        gw(glyph, 0x80000, "has non-integral points")
        gw(glyph, 0x100000, "has msissing anchors")
        gw(glyph, 0x200000, "has a duplicate glyph name")
        gw(glyph, 0x400000, "has a duplicate unicode code point")
        gw(glyph, 0x8000000, "has overlapping hints")
    if glyph.width == 0:
        warning("Glyph `" + glyph.glyphname + "` has a 0-width")

font.autoHint()

font.familyname += " Italic"
font.fullname = font.familyname
font.weight = "Regular"
font.generate(f"output/{output_filename}")
